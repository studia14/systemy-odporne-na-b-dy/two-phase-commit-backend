import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import pl.studia.tfc.entity.ClientManager;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.VoteRequest;
import pl.studia.tfc.tfccoordinator.VotingMessageSender;
import pl.studia.tfc.tfccoordinator.delegate.VotingDelegate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;

public class VotingMessageSenderTest {

    private VotingDelegate votingDelegate;
    private ClientManager clientManager;

    private ArgumentCaptor<Message> messageArgumentCaptor = ArgumentCaptor.forClass(Message.class);

    @BeforeEach
    public void init() {
        clientManager = Mockito.mock(ClientManager.class);
        votingDelegate = new VotingMessageSender(clientManager);
    }

    @Test
    public void shouldStartVote() {
        // given
        // when
        votingDelegate.startVoting(5);
        // then
        verify(clientManager).sendMessage(messageArgumentCaptor.capture());
        var argument = messageArgumentCaptor.getValue();
        assertThat(argument.getMessageType()).isEqualTo(MessageType.VOTE_REQUEST);
        assertThat(((VoteRequest)argument.getData()).getMessage()).isEqualTo(5);
    }

    @Test
    public void shouldAcceptState() {
        // given
        // when
        votingDelegate.newStateAccepted();
        // then
        verify(clientManager).sendMessage(messageArgumentCaptor.capture());
        var argument = messageArgumentCaptor.getValue();
        assertThat(argument.getMessageType()).isEqualTo(MessageType.GLOBAL_COMMIT);
    }

    @Test
    public void shouldRejectState() {
        // given
        // when
        votingDelegate.newStateRejected();
        // then
        verify(clientManager).sendMessage(messageArgumentCaptor.capture());
        var argument = messageArgumentCaptor.getValue();
        assertThat(argument.getMessageType()).isEqualTo(MessageType.GLOBAL_REJECT);
    }
}
