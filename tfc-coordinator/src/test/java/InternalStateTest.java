import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.studia.tfc.tfccoordinator.InternalState;
import pl.studia.tfc.tfccoordinator.delegate.GuiDelegate;
import pl.studia.tfc.tfccoordinator.delegate.TimeoutCounterDelegate;
import pl.studia.tfc.tfccoordinator.delegate.VotingDelegate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InternalStateTest {

    private VotingDelegate votingDelegate;
    private GuiDelegate guiDelegate;

    @BeforeEach
    public void prepareEnvironment() {
        this.votingDelegate = Mockito.mock(VotingDelegate.class);
        this.guiDelegate = Mockito.mock(GuiDelegate.class);
    }

    @Test
    public void shouldProperlyInitializeVotingStart() {
        // given
        var internalState = new InternalState(votingDelegate);
        internalState.setGuiDelegate(guiDelegate);
        var timeoutDelegate = Mockito.mock(TimeoutCounterDelegate.class);
        internalState.setTimeoutCounterDelegate(timeoutDelegate);
        // when
        internalState.startVoting(5);
        // then
        assertThat(internalState.getState()).isEqualTo(-1);
        assertThat(internalState.getStateProposition()).isEqualTo(5);
        verify(timeoutDelegate, times(1)).startCounter();
    }

    @Test
    public void shouldAddNewClient() {
        // given
        var internalState = new InternalState(votingDelegate);
        // then
        internalState.addNewClient();
        // then
        assertThat(internalState.getClientCount()).isEqualTo(1);
    }

    @Test
    public void shouldDisconnectClient() {
        // given
        var internalState = new InternalState(votingDelegate);
        internalState.addNewClient();;
        internalState.addNewClient();
        // when
        internalState.disconnectClient();
        // then
        assertThat(internalState.getClientCount()).isEqualTo(1);
    }

    @Test
    public void shouldReceivePositiveVote() {
        // given
        var internalState = new InternalState(votingDelegate);
        internalState.addNewClient();
        internalState.addNewClient();
        internalState.startVoting(5);
        // when
        internalState.receivePositiveVote();
        // then
        assertThat(internalState.getState()).isNotEqualTo(5);
        assertThat(internalState.getVoteCount()).isEqualTo(1);
    }

    @Test
    public void shouldReceiveNegativeVote() throws InterruptedException {
        // given
        var internalState = new InternalState(votingDelegate, 1);
        internalState.setGuiDelegate(guiDelegate);
        internalState.addNewClient();
        internalState.addNewClient();
        internalState.startVoting(5);
        // when
        internalState.receiveNegativeVote();
        // then
        Thread.sleep(10);
        assertThat(internalState.getState()).isNotEqualTo(5);
        Mockito.verify(guiDelegate, times(1)).newStateRejected();
        Mockito.verify(votingDelegate, times(1)).newStateRejected();
    }

    @Test
    public void shouldAcceptAfterReceiveTwoPositiveVotes() throws InterruptedException {
        // given
        // given
        var internalState = new InternalState(votingDelegate, 1);
        internalState.setGuiDelegate(guiDelegate);
        internalState.addNewClient();
        internalState.addNewClient();
        internalState.startVoting(5);
        // when
        internalState.receivePositiveVote();
        internalState.receivePositiveVote();
        // then
        Thread.sleep(10);
        Mockito.verify(guiDelegate, times(1)).newStateAccepted();
        Mockito.verify(votingDelegate, times(1)).newStateAccepted();
    }

    @Test
    public void shouldCallTimeoutDelegateAfterTimeout() {
        // given
        var internalState = new InternalState(votingDelegate);
        internalState.setGuiDelegate(guiDelegate);
        // when
        internalState.timeout();
        // then
        verify(votingDelegate, times(1)).newStateRejected();
        verify(guiDelegate, times(1)).newStateRejected();
    }
}
