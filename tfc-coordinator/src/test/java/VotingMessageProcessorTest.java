import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.studia.tfc.entity.ClientStatus;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.tfccoordinator.VotingListener;
import pl.studia.tfc.tfccoordinator.VotingMessageProcessor;
import pl.studia.tfc.tfccoordinator.delegate.GuiDelegate;

import java.net.Socket;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

public class VotingMessageProcessorTest {

    private VotingListener votingListener;
    private GuiDelegate guiDelegate;
    private VotingMessageProcessor votingMessageProcessor;

    @BeforeEach
    public void init() {
        votingListener = Mockito.mock(VotingListener.class);
        guiDelegate = Mockito.mock(GuiDelegate.class);
        votingMessageProcessor = new VotingMessageProcessor(votingListener);
        votingMessageProcessor.setGuiDelegate(guiDelegate);
    }

    @Test
    public void shouldParseAsPositiveVote() {
        // given
        var socket = Mockito.mock(Socket.class);
        var message = Message.builder()
                .messageType(MessageType.VOTE_COMMIT)
                .build();
        // when
        votingMessageProcessor.processMessage(message, socket);
        // then
        Mockito.verify(guiDelegate, times(1))
                .updateVoteStatus(socket, ClientStatus.VOTED_FOR_COMMIT);
        Mockito.verify(votingListener, times(1)).receivePositiveVote();
    }

    @Test
    public void shouldParseAsNegativeVote() {
        // given
        var socket = Mockito.mock(Socket.class);
        var message = Message.builder()
                .messageType(MessageType.VOTE_REJECT)
                .build();
        // when
        votingMessageProcessor.processMessage(message, socket);
        // then
        Mockito.verify(guiDelegate, times(1))
                .updateVoteStatus(socket, ClientStatus.VOTED_FOR_REJECT);
        Mockito.verify(votingListener, times(1)).receiveNegativeVote();
    }
}
