module tfc.coordinator {
    requires javafx.controls;
    requires javafx.fxml;
    requires tfc.common;
    requires static lombok;

    opens pl.studia.tfc.tfccoordinator to javafx.fxml;
    exports pl.studia.tfc.tfccoordinator;
    exports pl.studia.tfc.tfccoordinator.delegate;
    opens pl.studia.tfc.tfccoordinator.delegate to javafx.fxml;
}