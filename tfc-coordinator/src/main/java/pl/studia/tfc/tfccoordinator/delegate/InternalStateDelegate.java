package pl.studia.tfc.tfccoordinator.delegate;

public interface InternalStateDelegate {
    void startVoting(int newValue);
    void addNewClient();
    void disconnectClient();
}
