package pl.studia.tfc.tfccoordinator;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.entity.CountdownTimer;
import pl.studia.tfc.entity.CountdownTimerListener;
import pl.studia.tfc.tfccoordinator.delegate.GuiDelegate;
import pl.studia.tfc.tfccoordinator.delegate.TimeoutCounterDelegate;

@RequiredArgsConstructor
public class TimeoutCounterProcessor implements TimeoutCounterDelegate, CountdownTimerListener {

    private int timerLength = 30;
    private CountdownTimer countdownTimer = new CountdownTimer(this, timerLength);
    private final VotingListener votingListener;
    @Setter
    private GuiDelegate guiDelegate;

    @Override
    public void startCounter() {
        if (!countdownTimer.isActive()) {
            countdownTimer = new CountdownTimer(this, timerLength);
            countdownTimer.start();
        }
    }

    @Override
    public void stopCounter() {
        countdownTimer.cancel();
    }

    @Override
    public void onCountdownCancel() {

    }

    @Override
    public void onNormalCountdownEnd() {
        votingListener.timeout();
    }

    @Override
    public void onCountdownStep(int timer) {
        guiDelegate.updateTimeoutCounter((float)timer/timerLength);
    }
}
