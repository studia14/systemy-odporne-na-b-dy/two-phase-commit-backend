package pl.studia.tfc.tfccoordinator;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.studia.tfc.entity.*;
import pl.studia.tfc.tfccoordinator.message.ClientInfoProcessor;
import pl.studia.tfc.tfccoordinator.message.SimpleMessageLogger;

import java.io.IOException;
import java.util.Map;

public class CoordinatorApplication extends Application {

    private final MessageLogger messageLogger = new SimpleMessageLogger();
    private final ClientManager clientManager = new ClientManager(messageLogger);
    private final VotingMessageSender votingMessageSender = new VotingMessageSender(clientManager);
    private final InternalState internalState = new InternalState(votingMessageSender);
    private final VotingListener votingListener = new VotingListenerImpl(internalState);
    private final TimeoutCounterProcessor timeoutCounterProcessor = new TimeoutCounterProcessor(votingListener);
    private final VotingMessageProcessor votingMessageProcessor = new VotingMessageProcessor(votingListener);
    private final ObservableList<ConnectedServer> connectedClients = FXCollections.observableArrayList();
    ConnectionManager connectionManager;
    private final CoordinatorClientListener coordinatorClientListener = new CoordinatorClientListener(connectedClients, internalState);

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(CoordinatorApplication.class.getResource("coordinator-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);

        CoordinatorController coordinatorController = fxmlLoader.getController();
        coordinatorController.setInternalStateDelegate(internalState);
        coordinatorController.setConnectedClients(connectedClients);

        stage.setTitle("Koordynator");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();

        stage.setOnCloseRequest(a -> {
            onCloseJob();
        });
        internalState.setGuiDelegate(coordinatorController);
        internalState.setTimeoutCounterDelegate(timeoutCounterProcessor);
        timeoutCounterProcessor.setGuiDelegate(coordinatorController);
        votingMessageProcessor.setGuiDelegate(coordinatorController);

        Thread t = new Thread(() -> {
            connectionManager = new ConnectionManager(clientManager,
                    Map.of(MessageType.VOTE_COMMIT, votingMessageProcessor,
                            MessageType.VOTE_REJECT, votingMessageProcessor,
                            MessageType.NEW_CLIENT, new ClientInfoProcessor(clientManager)), coordinatorClientListener, messageLogger, 5000);
            connectionManager.run();
        });
        t.start();

    }

    public void onCloseJob(){
        Platform.exit();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch();
    }
}