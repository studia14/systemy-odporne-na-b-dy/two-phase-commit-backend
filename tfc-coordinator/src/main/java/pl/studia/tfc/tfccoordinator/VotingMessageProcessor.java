package pl.studia.tfc.tfccoordinator;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.entity.ClientStatus;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.processor.MessageProcessor;
import pl.studia.tfc.tfccoordinator.delegate.GuiDelegate;

import java.net.Socket;

@RequiredArgsConstructor
public class VotingMessageProcessor implements MessageProcessor {

    private final VotingListener votingListener;
    @Setter
    private GuiDelegate guiDelegate;

    @Override
    public <T extends Message> void processMessage(T message, Socket source) {
        if (message.getMessageType() == MessageType.VOTE_COMMIT) {
            guiDelegate.updateVoteStatus(source, ClientStatus.VOTED_FOR_COMMIT);
            votingListener.receivePositiveVote();
        } else if (message.getMessageType() == MessageType.VOTE_REJECT) {
            guiDelegate.updateVoteStatus(source, ClientStatus.VOTED_FOR_REJECT);
            votingListener.receiveNegativeVote();
        }
    }
}
