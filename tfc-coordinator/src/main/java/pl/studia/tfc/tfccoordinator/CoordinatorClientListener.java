package pl.studia.tfc.tfccoordinator;

import javafx.collections.ObservableList;
import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.ClientListener;
import pl.studia.tfc.entity.ClientStatus;
import pl.studia.tfc.entity.ConnectedServer;
import pl.studia.tfc.tfccoordinator.delegate.InternalStateDelegate;

import java.net.Socket;

@RequiredArgsConstructor
public class CoordinatorClientListener implements ClientListener {

    private final ObservableList<ConnectedServer> connectedClients;
    private final InternalStateDelegate internalStateDelegate;


    @Override
    public void onNewClientConnect(Socket socket) {
        connectedClients.add(new ConnectedServer(socket, ClientStatus.CONNECTED));
        internalStateDelegate.addNewClient();
    }

    @Override
    public void onClientDisconnect(Socket socket) {
        System.out.println("Client was disconnected");
        connectedClients.remove(new ConnectedServer(socket, null));
        internalStateDelegate.disconnectClient();
    }


}
