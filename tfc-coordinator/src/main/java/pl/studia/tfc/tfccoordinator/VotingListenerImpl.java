package pl.studia.tfc.tfccoordinator;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class VotingListenerImpl implements VotingListener {

    private final InternalState internalState;

    @Override
    public void receivePositiveVote() {
        internalState.receivePositiveVote();
    }

    @Override
    public void receiveNegativeVote() {
        internalState.receiveNegativeVote();
    }

    @Override
    public void timeout() {
        internalState.timeout();
    }
}
