package pl.studia.tfc.tfccoordinator.delegate;

public interface TimeoutCounterDelegate {
    void startCounter();
    void stopCounter();
}
