package pl.studia.tfc.tfccoordinator.message;

import pl.studia.tfc.entity.MessageLogger;

public class SimpleMessageLogger implements MessageLogger {
    @Override
    public void logMessage(String message) {
        System.out.println(message);
    }
}
