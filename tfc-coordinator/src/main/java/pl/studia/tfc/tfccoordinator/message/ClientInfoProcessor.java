package pl.studia.tfc.tfccoordinator.message;

import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.ClientManager;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.NewClientInfo;
import pl.studia.tfc.entity.processor.MessageProcessor;

import java.net.Socket;
import java.util.*;

@RequiredArgsConstructor
public class ClientInfoProcessor implements MessageProcessor {

    private final ClientManager clientManager;
    private final Map<Socket, NewClientInfo> alreadyConnectedClientInfos = new HashMap<>();

    @Override
    public <T extends Message> void processMessage(T message, Socket source) {
        var messageBody = (NewClientInfo)message.getData();
        System.out.println("New client connected: " + messageBody);
        clientManager.sendMessageExceptSource(message, messageBody.getLocalPort());
        alreadyConnectedClientInfos.forEach((key, value) ->
                clientManager.sendMessageToDestination(new Message(MessageType.NEW_CLIENT, value), source));
        alreadyConnectedClientInfos.put(source, messageBody);
    }
}
