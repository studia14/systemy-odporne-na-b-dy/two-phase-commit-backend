package pl.studia.tfc.tfccoordinator;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Setter;
import pl.studia.tfc.entity.ClientStatus;
import pl.studia.tfc.entity.ConnectedServer;
import pl.studia.tfc.tfccoordinator.delegate.GuiDelegate;
import pl.studia.tfc.tfccoordinator.delegate.InternalStateDelegate;

import java.net.Socket;
import java.util.List;

public class CoordinatorController implements GuiDelegate {

    @FXML
    private Label votingResultText;

    @FXML
    private TableView<ConnectedServer> connectedServersTableView;

    @FXML
    private TableColumn<ConnectedServer, String> serverColumn;

    @FXML
    private TableColumn<ConnectedServer, String> statusColumn;

    @FXML
    public TableColumn<ConnectedServer, String> lastActivityColumn;

    @FXML
    private Label currentValueText;

    @FXML
    private Button startVotingButton;

    @FXML
    private ProgressBar votingTimeoutBar;

    @FXML
    private TextField newValueInput;

    @Setter
    private InternalStateDelegate internalStateDelegate;

    private ObservableList<ConnectedServer> connectedClients;

    public void setConnectedClients(ObservableList<ConnectedServer> connectedClients) {
        this.connectedClients = connectedClients;
        connectedServersTableView.setItems(connectedClients);
    }

    @FXML
    public void startVoting() {
        votingTimeoutBar.setProgress(1.0);
        int newValue = Integer.parseInt(newValueInput.getText());
        internalStateDelegate.startVoting(newValue);
    }


    @Override
    public void disableVoting() {
        Platform.runLater(() -> {
            startVotingButton.setDisable(true);
        });
    }

    @Override
    public void enableVoting() {
        Platform.runLater(() -> {
            startVotingButton.setDisable(false);
        });
    }

    @Override
    public void newStateAccepted() {
        Platform.runLater(() -> {
            currentValueText.setText(newValueInput.getText());
            votingResultText.setText("Globalne zatwierdzenie");
        });
        Platform.runLater( () -> {
            currentValueText.setText(newValueInput.getText());
        });
        enableVoting();
    }

    @Override
    public void newStateRejected() {
        Platform.runLater(() -> {
            votingResultText.setText("Globalne odrzucenie");
        });
    }

    @FXML
    public void initialize() {

        currentValueText.setText("");
        votingResultText.setText("");
        serverColumn.setCellValueFactory(new PropertyValueFactory<>("connectionInfo"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        lastActivityColumn.setCellValueFactory(new PropertyValueFactory<>("lastActivity"));
    }

    @Override
    public void updateTimeoutCounter(double timeLeftPercent) {
        assert timeLeftPercent <= 1 && timeLeftPercent >= 0;
        Platform.runLater(() -> {
            votingTimeoutBar.setProgress(timeLeftPercent);
        });
    }

    @Override
    public void setWaitingForVoteStatus() {
        for (ConnectedServer client : connectedClients) {
            updateVoteStatus(client.getSocket(), ClientStatus.WAITING_FOR_VOTE);
        }
    }

    @Override
    public void setVoteIgnoredForWaitingForVoteServers() {
        for (ConnectedServer server : connectedClients) {
            if (server.getStatus().equals(ClientStatus.WAITING_FOR_VOTE)) {
                updateVoteStatus(server.getSocket(), ClientStatus.VOTE_IGNORED);
            }
        }
    }

    @Override
    public void setTimeoutForWaitingForVoteServers() {
        for (ConnectedServer server : connectedClients) {
            if (server.getStatus().equals(ClientStatus.WAITING_FOR_VOTE))
                updateVoteStatus(server.getSocket(), ClientStatus.VOTING_TIMEOUT);
        }
    }

    @Override
    public void updateVoteStatus(Socket serverSocket, ClientStatus newStatus) {
        ConnectedServer connectedServer = connectedClients.get(connectedClients.indexOf(new ConnectedServer(serverSocket, null)));
        if (connectedServer.getStatus() != ClientStatus.VOTE_IGNORED
                || List.of(ClientStatus.WAITING_FOR_VOTE, ClientStatus.CONNECTED).contains(newStatus)) {
            connectedServer.setStatus(newStatus);
            connectedServersTableView.refresh();
        }
    }
}