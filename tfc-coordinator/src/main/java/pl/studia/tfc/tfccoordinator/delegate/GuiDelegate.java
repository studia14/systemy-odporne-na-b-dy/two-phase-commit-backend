package pl.studia.tfc.tfccoordinator.delegate;

import pl.studia.tfc.entity.ClientStatus;

import java.net.Socket;

public interface GuiDelegate {
    void disableVoting();
    void enableVoting();
    void newStateAccepted();
    void newStateRejected();
    void updateTimeoutCounter(double timeLeftPercent);
    void setWaitingForVoteStatus();
    void setVoteIgnoredForWaitingForVoteServers();
    void setTimeoutForWaitingForVoteServers();
    void updateVoteStatus(Socket client, ClientStatus status);
}
