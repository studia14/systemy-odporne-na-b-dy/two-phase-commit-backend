package pl.studia.tfc.tfccoordinator;

import lombok.Getter;
import lombok.Setter;
import pl.studia.tfc.tfccoordinator.delegate.GuiDelegate;
import pl.studia.tfc.tfccoordinator.delegate.InternalStateDelegate;
import pl.studia.tfc.tfccoordinator.delegate.TimeoutCounterDelegate;
import pl.studia.tfc.tfccoordinator.delegate.VotingDelegate;

import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class InternalState implements InternalStateDelegate {
    @Getter
    private int stateProposition = -1;
    @Getter
    private int state = -1;
    @Getter
    private int clientCount = 0;
    @Getter
    private int voteCount = 0;
    private final VotingDelegate votingDelegate;
    private final Random random = new Random();
    @Setter
    private TimeoutCounterDelegate timeoutCounterDelegate;
    @Setter
    private GuiDelegate guiDelegate;
    private int waitingTime = 9000;

    public InternalState(VotingDelegate votingDelegate) {
        this.votingDelegate = votingDelegate;
    }

    public InternalState(VotingDelegate votingDelegate, int waitingTime) {
        this.votingDelegate = votingDelegate;
        this.waitingTime = waitingTime;
    }

    @Override
    public void addNewClient() {
        clientCount++;
    }

    @Override
    public void disconnectClient() {
        clientCount--;
    }

    @Override
    public void startVoting(int newState) {
        stateProposition = newState;
        voteCount = 0;
        votingDelegate.startVoting(newState);
        Optional.ofNullable(timeoutCounterDelegate).ifPresent(TimeoutCounterDelegate::startCounter);
        Optional.ofNullable(guiDelegate).ifPresent(GuiDelegate::setWaitingForVoteStatus);
    }

    public void receivePositiveVote() {
        voteCount++;
        if(voteCount == clientCount) {
            state = stateProposition;
            Executors.newSingleThreadScheduledExecutor().schedule(votingDelegate::newStateAccepted, random.nextInt(waitingTime), TimeUnit.MILLISECONDS);
            guiDelegate.newStateAccepted();
            Optional.ofNullable(timeoutCounterDelegate).ifPresent(TimeoutCounterDelegate::stopCounter);
        }
    }

    public void receiveNegativeVote() {
        guiDelegate.setVoteIgnoredForWaitingForVoteServers();
        Executors.newSingleThreadScheduledExecutor().schedule(votingDelegate::newStateRejected, random.nextInt(waitingTime), TimeUnit.MILLISECONDS);

        guiDelegate.enableVoting();
        guiDelegate.newStateRejected();
        Optional.ofNullable(timeoutCounterDelegate).ifPresent(TimeoutCounterDelegate::stopCounter);
    }

    public void timeout() {
        votingDelegate.newStateRejected();
        guiDelegate.newStateRejected();
    }
}
