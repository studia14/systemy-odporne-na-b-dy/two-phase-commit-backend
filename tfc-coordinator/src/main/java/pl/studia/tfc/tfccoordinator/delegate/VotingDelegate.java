package pl.studia.tfc.tfccoordinator.delegate;

public interface VotingDelegate {
    void startVoting(int newState);
    void newStateAccepted();
    void newStateRejected();
    void timeout();
}
