package pl.studia.tfc.tfccoordinator;

import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.*;
import pl.studia.tfc.tfccoordinator.delegate.VotingDelegate;

import java.util.UUID;

import static pl.studia.tfc.entity.MessageType.VOTE_REQUEST;

@RequiredArgsConstructor
public class VotingMessageSender implements VotingDelegate {

    private final ClientManager clientManager;

    @Override
    public void startVoting(int newState) {
        MessageBody messageBody = new VoteRequest(UUID.randomUUID().toString(), newState);
        Message message = new Message(VOTE_REQUEST, messageBody);
        clientManager.sendMessage(message);
    }

    @Override
    public void newStateAccepted() {
        Message message = new Message(MessageType.GLOBAL_COMMIT, null);
        clientManager.sendMessage(message);
    }

    @Override
    public void newStateRejected() {
        Message message = new Message(MessageType.GLOBAL_REJECT, null);
        clientManager.sendMessage(message);
    }

    @Override
    public void timeout() {

    }
}
