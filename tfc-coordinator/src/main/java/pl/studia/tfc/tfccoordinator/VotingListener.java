package pl.studia.tfc.tfccoordinator;

public interface VotingListener {
    void receivePositiveVote();
    void receiveNegativeVote();
    void timeout();
}
