package pl.studia.tfc.entity;

import java.net.Socket;

public interface ClientListener {
    void onNewClientConnect(Socket socket);
    void onClientDisconnect(Socket socket);
}
