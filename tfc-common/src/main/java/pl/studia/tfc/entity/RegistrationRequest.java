package pl.studia.tfc.entity;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RegistrationRequest implements MessageBody {
    private String ipAddress;
    private Integer port;
    private Integer serverIdentifierNumber;
}
