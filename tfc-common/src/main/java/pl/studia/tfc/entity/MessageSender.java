package pl.studia.tfc.entity;

import lombok.Setter;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class MessageSender {
    private final Socket socket;
    private final MessageLogger messageLogger;
    private ObjectOutputStream ouStream = null;

    public int getPort() {
        return socket.getPort();
    }

    public MessageSender(Socket socket, MessageLogger messageLogger) {
        this.socket = socket;
        this.messageLogger = messageLogger;
        try {
            var outputStream = this.socket.getOutputStream();
            ouStream = new ObjectOutputStream(outputStream);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isSameSocket(Socket socket) {
        return socket.equals(this.socket);
    }

    public <T extends Message> void sendMessage(T message) {
        try {
            if(ouStream != null) {
                Optional.ofNullable(messageLogger).ifPresent(messageLogger -> {
                    messageLogger.logMessage(String.format("%s Send - Thread: %s Destination: %s, message: %s",
                            LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                            Thread.currentThread().getId(), socket.getRemoteSocketAddress().toString(), message));
                });
                ouStream.writeObject(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
