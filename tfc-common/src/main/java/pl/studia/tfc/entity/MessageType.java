package pl.studia.tfc.entity;

public enum MessageType {
    VOTE_REQUEST,
    VOTE_COMMIT,
    VOTE_REJECT,
    GLOBAL_COMMIT,
    GLOBAL_REJECT,
    REGISTER_CLIENT,
    REQUEST_FOR_INFORMATION,
    REQUEST_FOR_INFORMATION_RESPONSE,
    NEW_CLIENT
}
