package pl.studia.tfc.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.entity.processor.MessageProcessor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.SocketException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

@Getter
@RequiredArgsConstructor
public class MessageReceiver extends Thread {

    private final Socket socket;

    private final Map<MessageType, MessageProcessor> messageTypeToProcessorMap;

    private final ClientListener clientListener;
    private final MessageLogger messageLogger;

    @Override
    public void run() {
        try {
            var inputStream = this.socket.getInputStream();
            var oiStream = new ObjectInputStream(inputStream);
            while(true) {
                try {
                    var message = (Message)oiStream.readObject();
                    Optional.ofNullable(messageLogger).ifPresent(messageLogger -> {
                        messageLogger.logMessage(String.format("%s Receive message Thread: %s Source: %s, message: %s",
                                LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                                Thread.currentThread().getId(), socket.getRemoteSocketAddress().toString(), message));
                    });
                    messageTypeToProcessorMap.get(message.getMessageType()).processMessage(message, socket);

                }
                catch(SocketException socketException) {
                    clientListener.onClientDisconnect(socket);
                    return;
                }
                catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
