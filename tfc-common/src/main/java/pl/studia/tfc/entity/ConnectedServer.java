package pl.studia.tfc.entity;

import lombok.AllArgsConstructor;
import lombok.Setter;

import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

//@AllArgsConstructor
//@Setter
public class ConnectedServer {

    private final static DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    private final Socket connectionInfo;
    private ClientStatus status;
    private Date lastActivity;

    public String getConnectionInfo() {
        return connectionInfo.getLocalAddress() + ":" + connectionInfo.getPort();
    }

    public Socket getSocket() {
        return connectionInfo;
    }

    public ClientStatus getStatus() {
        return status;
    }

    public String getLastActivity() {
        return dateFormat.format(lastActivity);
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
        this.lastActivity = new Date();
    }

    public ConnectedServer(Socket connectionInfo, ClientStatus status) {
        this.connectionInfo = connectionInfo;
        this.status = status;
        this.lastActivity = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConnectedServer that = (ConnectedServer) o;
        return connectionInfo.equals(that.connectionInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionInfo);
    }
}
