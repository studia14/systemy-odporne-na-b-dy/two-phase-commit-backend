package pl.studia.tfc.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor(staticName = "of")
@ToString
public class RequestForInformationResponse implements MessageBody {
    private int commitedValue;
    private boolean isCommited;
}
