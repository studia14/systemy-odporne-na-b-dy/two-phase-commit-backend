package pl.studia.tfc.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor(staticName = "of")
@ToString
public class NewClientInfo implements MessageBody {
    private String ipAddress;
    private Integer port;
    private Integer localPort;
    private String identifier;
}
