package pl.studia.tfc.entity;

public enum ClientStatus {
    VOTED_FOR_COMMIT("Głos za zatwierdzeniem"),
    VOTED_FOR_REJECT("Głos za odrzuceniem"),
    VOTE_IGNORED("Głos ignorowany"),
    VOTING_TIMEOUT("Timeout - odrzucenie"),
    WAITING_FOR_VOTE("Oczekiwanie na głos"),
    CONNECTED("Połączony");

    private final String name;

    ClientStatus(String s) {
        name = s;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
