package pl.studia.tfc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class VoteResponse implements MessageBody {
}
