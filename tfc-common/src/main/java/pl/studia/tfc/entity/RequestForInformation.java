package pl.studia.tfc.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor(staticName = "of")
@ToString
public class RequestForInformation implements MessageBody {
    private String identifier;
    private String voteIdentifier;
}
