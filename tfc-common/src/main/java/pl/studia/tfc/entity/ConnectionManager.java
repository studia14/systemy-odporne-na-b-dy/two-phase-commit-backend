package pl.studia.tfc.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import pl.studia.tfc.entity.ClientListener;
import pl.studia.tfc.entity.MessageReceiver;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.processor.MessageProcessor;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Map;

public class ConnectionManager implements Runnable {

    private final ClientManager clientManager;
    private final Map<MessageType, MessageProcessor> messageMap;
    private final ClientListener clientListener;
    private final MessageLogger messageLogger;
    @Getter
    private final int port;

    @Getter
    private ServerSocket serverSocket;

    @SneakyThrows
    public ConnectionManager(ClientManager clientManager, Map<MessageType, MessageProcessor> messageMap, ClientListener clientListener, MessageLogger messageLogger, int port) {
        this.clientManager = clientManager;
        this.messageMap = messageMap;
        this.clientListener = clientListener;
        this.port = port;
        this.serverSocket = new ServerSocket(port);
        this.messageLogger = messageLogger;
    }

    public void run() {
        while (true) {
            try {
                var socket = serverSocket.accept();
                clientManager.addConnection(socket);
                clientListener.onNewClientConnect(socket);
                new MessageReceiver(socket, messageMap, clientListener, messageLogger).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
