package pl.studia.tfc.entity;

public enum VoteType {
    COMMIT,
    REJECT
}
