package pl.studia.tfc.entity;

import lombok.Getter;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageSender;

import java.net.Socket;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

//@RequiredArgsConstructor
public class ClientManager {
    private final Set<Socket> clients;
    private final Random random = new Random();

    @Getter
    private final Set<MessageSender> messageSenders;

    private final MessageLogger messageLogger;

    public ClientManager(MessageLogger messageLogger) {
        this.clients = new HashSet<>();
        this.messageSenders = new HashSet<>();
        this.messageLogger = messageLogger;
    }

    void addConnection(Socket socket) {
        clients.add(socket);
        messageSenders.add(new MessageSender(socket, messageLogger));
    }

    public void sendMessage(Message message) {
        messageSenders.forEach(messageSender -> {
            messageSender.sendMessage(message);
        });
    }

    public void sendMessageToSomeOf(Message message) {
        messageSenders.forEach(messageSender -> {
            if(random.nextInt(10) < 7) {
                messageSender.sendMessage(message);
            }
        });
    }

    public void sendMessageToDestination(Message message, Socket destination) {
        messageSenders.stream()
                .filter(messageSender -> messageSender.isSameSocket(destination))
                .findFirst()
                .ifPresent((messageSender -> {
                    messageSender.sendMessage(message);
                }));
    }

    public void sendMessageExceptSource(Message message, int localPort) {
        messageSenders.stream()
                .filter(messageSender -> messageSender.getPort() != localPort)
                .forEach(messageSender -> messageSender.sendMessage(message));
    }
}
