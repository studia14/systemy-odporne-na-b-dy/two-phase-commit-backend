package pl.studia.tfc.entity.processor;

import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageBody;

import java.net.Socket;

public interface MessageProcessor {
    <T extends Message> void processMessage(T message, Socket source);
}
