package pl.studia.tfc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Builder
@AllArgsConstructor
@ToString
public class Message implements Serializable {
    private MessageType messageType;
    private MessageBody data;
}