package pl.studia.tfc.entity;

public interface CountdownTimerListener {
    void onCountdownCancel();
    void onNormalCountdownEnd();
    void onCountdownStep(int timer);
}
