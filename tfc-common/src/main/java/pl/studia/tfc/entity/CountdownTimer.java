package pl.studia.tfc.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
public class CountdownTimer extends Thread {

    private final CountdownTimerListener countdownTimerListener;
    private int timer = 60;
    public final static int countdownTimerLength = 30;

    public CountdownTimer(CountdownTimerListener countdownTimerListener, int timer) {
        this.countdownTimerListener = countdownTimerListener;
        this.timer = timer;
    }

    private boolean canceled = false;

    @Getter
    private boolean active = false;

    public void cancel() {
        active = false;
        canceled = true;
    }

    @Override
    public void run() {
        while (true) {
            active = true;
            timer--;
            countdownTimerListener.onCountdownStep(timer);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (canceled) {
                countdownTimerListener.onCountdownCancel();
                active = false;
                return;
            }
            if (timer <= 0) {
                countdownTimerListener.onNormalCountdownEnd();
                active = false;
                return;
            }
        }
    }
}
