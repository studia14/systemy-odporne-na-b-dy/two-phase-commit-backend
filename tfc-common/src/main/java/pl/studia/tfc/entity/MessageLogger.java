package pl.studia.tfc.entity;

public interface MessageLogger {
    void logMessage(String message);
}
