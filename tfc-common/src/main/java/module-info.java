module tfc.common {
    requires static lombok;
    exports pl.studia.tfc.entity;
    exports pl.studia.tfc.entity.processor;
}