import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.VoteRequest;
import pl.studia.tfc.tfcserver.InternalState;
import pl.studia.tfc.tfcserver.TimeoutCounterDelegate;
import pl.studia.tfc.tfcserver.delegate.GuiDelegate;
import pl.studia.tfc.tfcserver.delegate.OtherClientsInfoDelegate;
import pl.studia.tfc.tfcserver.delegate.VotingDelegate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;

public class InternalStateTest {
    private VotingDelegate votingDelegate;
    private GuiDelegate guiDelegate;
    private TimeoutCounterDelegate timeoutDelegate;
    private OtherClientsInfoDelegate otherClientsInfoDelegate;
    private InternalState internalState;

    @BeforeEach
    public void prepareEnvironment() {
        this.votingDelegate = Mockito.mock(VotingDelegate.class);
        this.guiDelegate = Mockito.mock(GuiDelegate.class);
        this.timeoutDelegate = Mockito.mock(TimeoutCounterDelegate.class);
        this.otherClientsInfoDelegate = Mockito.mock(OtherClientsInfoDelegate.class);
        internalState = new InternalState(votingDelegate, timeoutDelegate, otherClientsInfoDelegate);
        internalState.setGuiDelegate(guiDelegate);
    }

    @Test
    public void shouldBeReadyToAcceptVoting() {
        // given
        // when
        internalState.acceptVoting(true);
        // then
        assertTrue(internalState.isAcceptVoting());
        assertThat(internalState.getValueProposition()).isEqualTo(-1);
        assertThat(internalState.getValue()).isEqualTo(-1);
        assertFalse(internalState.isVoteStarted());
        assertFalse(internalState.isHangup());
    }

    @Test
    public void shouldVote() {
        // given
        var messageBody = Mockito.mock(VoteRequest.class);
        var message = Message.builder()
                .messageType(MessageType.VOTE_REQUEST)
                .data(messageBody)
                .build();
        // when
        internalState.vote(
                ((VoteRequest) message.getData()).getMessage(),
                ((VoteRequest) message.getData()).getVoteIdentifier()
        );
        // then
        assertThat(messageBody.getMessage()).isEqualTo(internalState.getValueProposition());
        assertThat(messageBody.getVoteIdentifier()).isEqualTo(internalState.getLastVotingUUID());
        Mockito.verify(timeoutDelegate, times(1))
                .startCounter();
        Mockito.verify(guiDelegate, times(1))
                .startVoting(messageBody.getMessage());
    }
}
