import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.tfcserver.VotingMessageSender;
import pl.studia.tfc.tfcserver.delegate.VotingDelegate;
import pl.studia.tfc.tfcserver.tcp.CoordinatorClient;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;

public class VotingMessageSenderTest {
    private VotingDelegate votingDelegate;
    private CoordinatorClient coordinatorClient;

    private ArgumentCaptor<Message> messageArgumentCaptor = ArgumentCaptor.forClass(Message.class);

    @BeforeEach
    public void init() {
        coordinatorClient = Mockito.mock(CoordinatorClient.class);
        votingDelegate = new VotingMessageSender(coordinatorClient);
    }

    @Test
    public void shouldVoteForCommit() {
        // given
        // when
        votingDelegate.voteForCommit();
        // then
        verify(coordinatorClient).sendMessage(messageArgumentCaptor.capture());
        var argument = messageArgumentCaptor.getValue();
        assertThat(argument.getMessageType()).isEqualTo(MessageType.VOTE_COMMIT);
    }

    @Test
    public void shouldVoteForReject() {
        // given
        // when
        votingDelegate.voteForReject();
        // then
        verify(coordinatorClient).sendMessage(messageArgumentCaptor.capture());
        var argument = messageArgumentCaptor.getValue();
        assertThat(argument.getMessageType()).isEqualTo(MessageType.VOTE_REJECT);
    }
}
