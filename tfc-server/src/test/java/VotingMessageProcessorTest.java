import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.VoteRequest;
import pl.studia.tfc.tfcserver.VotingListener;
import pl.studia.tfc.tfcserver.VotingMessageProcessor;

import java.net.Socket;

import static org.mockito.Mockito.times;

public class VotingMessageProcessorTest {
    private VotingListener votingListener;
    private VotingMessageProcessor votingMessageProcessor;

    @BeforeEach
    public void init() {
        votingListener = Mockito.mock(VotingListener.class);
        votingMessageProcessor = new VotingMessageProcessor();
        votingMessageProcessor.addVotingListener(votingListener);
    }

    @Test
    public void shouldProcessVotingRequest() {
        // given
        var socket = Mockito.mock(Socket.class);
        var messageBody = Mockito.mock(VoteRequest.class);
        var message = Message.builder()
                .messageType(MessageType.VOTE_REQUEST)
                .data(messageBody)
                .build();
        // when
        votingMessageProcessor.processMessage(message, socket);
        // then
        Mockito.verify(votingListener, times(1))
                .receiveVotingRequest(((VoteRequest) message.getData()).getMessage(), ((VoteRequest) message.getData()).getVoteIdentifier());
    }

    @Test
    public void shouldProcessGlobalCommit() {
        // given
        var socket = Mockito.mock(Socket.class);
        var message = Message.builder()
                .messageType(MessageType.GLOBAL_COMMIT)
                .build();
        // when
        votingMessageProcessor.processMessage(message, socket);
        // then
        Mockito.verify(votingListener, times(1))
                .globalAccept();
    }

    @Test
    public void shouldProcessGlobalReject() {
        // given
        var socket = Mockito.mock(Socket.class);
        var message = Message.builder()
                .messageType(MessageType.GLOBAL_REJECT)
                .build();
        // when
        votingMessageProcessor.processMessage(message, socket);
        // then
        Mockito.verify(votingListener, times(1))
                .globalReject();
    }
}
