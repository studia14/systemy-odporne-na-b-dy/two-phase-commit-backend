package pl.studia.tfc.tfcserver;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.studia.tfc.entity.*;
import pl.studia.tfc.entity.processor.MessageProcessor;
import pl.studia.tfc.tfcserver.message.NewClientConnectedProcessor;
import pl.studia.tfc.tfcserver.tcp.CoordinatorClient;
import pl.studia.tfc.tfcserver.tcp.ServerClientListener;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ServerApplication extends Application {

    private final String SERVER_UUID = UUID.randomUUID().toString();
    private static final String SERVER_HOSTNAME = "127.0.0.1";
    private static final Integer SERVER_PORT = 5000;
    private final MessageLogger messageLogger = new SimpleMessageLogger();
    private final ClientManager clientManager = new ClientManager(messageLogger);
    private final Map<String, CoordinatorClient> otherClientsCoordinators = new HashMap<>();
    private final VotingMessageProcessor votingMessageProcessor = new VotingMessageProcessor();
    private final ServerClientListener serverClientListener = new ServerClientListener();
    private final CoordinatorClient coordinatorClient =  new CoordinatorClient(
            Map.of(MessageType.VOTE_REQUEST, votingMessageProcessor,
                    MessageType.GLOBAL_COMMIT, votingMessageProcessor,
                    MessageType.GLOBAL_REJECT, votingMessageProcessor,
                    MessageType.NEW_CLIENT, new NewClientConnectedProcessor(otherClientsCoordinators)), serverClientListener);
    private final VotingMessageSender votingMessageSender = new VotingMessageSender(coordinatorClient);
    private final TimeoutCounterProcessor timeoutCounterProcessor = new TimeoutCounterProcessor();

    private final OtherClientsInfoMessageSender otherClientsInfoMessageSender = new OtherClientsInfoMessageSender(SERVER_UUID, otherClientsCoordinators);
    private final InternalState internalState = new InternalState(votingMessageSender, timeoutCounterProcessor, otherClientsInfoMessageSender);
    private final VotingListener votingListener = new VotingListenerImpl(internalState);
    private final OtherClientsMessageProcessor otherClientsMessageProcessor = new OtherClientsMessageProcessor();
    private final ConnectionManager connectionManager = new ConnectionManager(clientManager,
            Map.of(MessageType.REQUEST_FOR_INFORMATION, otherClientsMessageProcessor,
                    MessageType.REQUEST_FOR_INFORMATION_RESPONSE, otherClientsMessageProcessor),
            serverClientListener, new SimpleMessageLogger(), 0);

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ServerApplication.class.getResource("server-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Serwer");
        stage.setScene(scene);
        ServerController serverController = fxmlLoader.getController();
        stage.centerOnScreen();
        stage.show();

        stage.setOnCloseRequest(a -> {
            onCloseJob();
        });

        timeoutCounterProcessor.setVotingListener(votingListener);
        timeoutCounterProcessor.setGuiDelegate(serverController);
        votingMessageProcessor.addVotingListener(votingListener);
        internalState.setGuiDelegate(serverController);
        serverController.setPropertiesChangeListener(internalState);
        otherClientsMessageProcessor.setOtherClientsInfoListener(internalState);


        Thread t = new Thread(() -> {
            coordinatorClient.connect(SERVER_HOSTNAME, SERVER_PORT);
        });

        Thread t2 = new Thread(() -> {
            while (!coordinatorClient.isConnected()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            var message = new Message(MessageType.NEW_CLIENT, NewClientInfo.of(connectionManager.getServerSocket().getInetAddress().getHostAddress(), connectionManager.getServerSocket().getLocalPort(), coordinatorClient.getLocalPort(), SERVER_UUID));
            coordinatorClient.sendMessage(message);
            connectionManager.run();

        });

        t.start();
        t2.start();

    }

    public void onCloseJob(){
        Platform.exit();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch();
    }
}