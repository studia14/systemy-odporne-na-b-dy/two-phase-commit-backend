package pl.studia.tfc.tfcserver;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import lombok.Setter;
import pl.studia.tfc.tfcserver.delegate.GuiDelegate;
import pl.studia.tfc.tfcserver.delegate.PropertiesChangeListener;


public class ServerController implements GuiDelegate {

    @Setter
    private PropertiesChangeListener propertiesChangeListener;

    @FXML
    private Label votingResultText;

    @FXML
    private Label connectionStatusText;

    @FXML
    private CheckBox ignoreGlobalAcceptRejectMessageCheckbox;

    @FXML
    private CheckBox votingForRejectCheckbox;

    @FXML
    private Label currentValueText;

    @FXML
    private CheckBox ignoreVoteRequestMessageCheckbox;

    @FXML
    private ProgressBar votingTimeoutBar;

    @FXML
    private CheckBox hangupCheckbox;

    @FXML
    private Label newValueText;

    @FXML
    private GridPane votingPane;

    @FXML
    private HBox newValueBox;

    @Override
    public void startVoting(int newValue) {
        Platform.runLater(() -> {
            newValueText.setText(String.valueOf(newValue));
            newValueBox.setDisable(false);
            votingResultText.setText("Oczekiwanie na oddanie głosu");
            if (ignoreGlobalAcceptRejectMessageCheckbox.isSelected()) {
                finishVoting(); // IGNORE voting if
            }
        });
    }

    private void finishVoting() {
        newValueBox.setDisable(true);
    }

    @Override
    public void gotGlobalCommit(int newValue) {
        Platform.runLater(() -> {
            votingResultText.setText("Globalne zatwierdzenie");
            currentValueText.setText(newValueText.getText());
        });
    }

    @Override
    public void gotGlobalReject() {
        Platform.runLater(() -> {
            finishVoting();
            votingResultText.setText("Globalne odrzucenie");
        });
    }

    @Override
    public void doneVoting() {
        Platform.runLater(() -> {
            votingResultText.setText("Oczekiwanie na rezultat głosowania");
            finishVoting();
        });
    }

    @Override
    public void updateCountdown(double timeLeftPercent) {
        assert timeLeftPercent <= 1 && timeLeftPercent >= 0;
        Platform.runLater(() -> votingTimeoutBar.setProgress(timeLeftPercent));
    }

    @Override
    public void timeoutOccurred() {
        Platform.runLater(() -> {
            finishVoting();
            votingResultText.setText("Timeout");
        });
    }

    @FXML
    public void initialize() {
        votingForRejectCheckbox.selectedProperty().addListener((observableValue, aBoolean, t1) -> propertiesChangeListener.acceptVoting(!observableValue.getValue()));
        ignoreVoteRequestMessageCheckbox.selectedProperty().addListener((observableValue, aBoolean, t1) -> propertiesChangeListener.blockRequestForVotingMessage(observableValue.getValue()));
        ignoreGlobalAcceptRejectMessageCheckbox.selectedProperty().addListener(((observableValue, aBoolean, t1) -> propertiesChangeListener.ignoreGlobalAcceptRejectMessage(observableValue.getValue())));
        hangupCheckbox.selectedProperty().addListener(((observableValue, aBoolean, t1) -> propertiesChangeListener.hangUp(observableValue.getValue())));
        votingResultText.setText("");
        currentValueText.setText("");
        newValueText.setText("");
    }
}