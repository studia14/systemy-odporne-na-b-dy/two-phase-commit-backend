package pl.studia.tfc.tfcserver;

import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.tfcserver.delegate.VotingDelegate;
import pl.studia.tfc.tfcserver.tcp.CoordinatorClient;

import java.util.Optional;

@RequiredArgsConstructor
public class VotingMessageSender implements VotingDelegate {

    private final CoordinatorClient coordinatorClient;

    @Override
    public void voteForCommit() {
        Message message = new Message(MessageType.VOTE_COMMIT, null);
        Optional.ofNullable(coordinatorClient).ifPresent(client -> client.sendMessage(message));
    }

    @Override
    public void voteForReject() {
        Message message = new Message(MessageType.VOTE_REJECT, null);
        Optional.ofNullable(coordinatorClient).ifPresent(client -> client.sendMessage(message));
    }
}
