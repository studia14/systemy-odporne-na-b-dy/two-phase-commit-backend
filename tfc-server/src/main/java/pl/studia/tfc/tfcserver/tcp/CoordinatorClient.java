package pl.studia.tfc.tfcserver.tcp;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.*;
import pl.studia.tfc.entity.processor.MessageProcessor;
import pl.studia.tfc.tfcserver.SimpleMessageLogger;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;

@RequiredArgsConstructor
public class CoordinatorClient {
    private final Map<MessageType, MessageProcessor> messageTypeProcesorMap;
    private MessageSender messageSender;
    private MessageReceiver messageReceiver;
    private MessageLogger messageLogger = new SimpleMessageLogger();
    private Socket socket;
    private final ServerClientListener serverClientListener;

    @Getter
    private boolean connected = false;

    public int getLocalPort() {
        return socket.getLocalPort();
    }

    public void connect(String ipAddress, Integer port) {
        try {
            this.socket = new Socket(ipAddress, port);
            messageSender = new MessageSender(socket, messageLogger);
            messageReceiver = new MessageReceiver(socket, messageTypeProcesorMap,serverClientListener, messageLogger);
            messageReceiver.start();
            connected = true;
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    public <T extends Message> void sendMessage(T message) {
        messageSender.sendMessage(message);
    }
}
