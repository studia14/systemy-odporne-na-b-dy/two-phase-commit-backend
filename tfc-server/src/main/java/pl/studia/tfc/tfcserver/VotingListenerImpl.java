package pl.studia.tfc.tfcserver;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class VotingListenerImpl implements VotingListener {

    private final InternalState internalState;

    @Override
    public void receiveVotingRequest(int stateProposition, String votingUUID) {
        internalState.vote(stateProposition, votingUUID);
    }

    @Override
    public void globalAccept() {
        internalState.globalAccept();
    }

    @Override
    public void globalReject() {
        internalState.globalReject();
    }

    @Override
    public void timeout() {
        internalState.timeout();
    }

    @Override
    public void timeoutStep() {
        internalState.timeoutStep();
    }
}
