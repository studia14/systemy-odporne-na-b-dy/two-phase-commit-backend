package pl.studia.tfc.tfcserver;

public interface VotingListener {
    void receiveVotingRequest(int stateProposition, String votingUUID);
    void globalAccept();
    void globalReject();
    void timeout();
    void timeoutStep();
}
