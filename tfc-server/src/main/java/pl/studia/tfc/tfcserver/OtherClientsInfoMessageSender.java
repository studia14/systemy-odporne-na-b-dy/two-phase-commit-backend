package pl.studia.tfc.tfcserver;

import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.RequestForInformation;
import pl.studia.tfc.entity.RequestForInformationResponse;
import pl.studia.tfc.tfcserver.delegate.OtherClientsInfoDelegate;
import pl.studia.tfc.tfcserver.tcp.CoordinatorClient;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RequiredArgsConstructor
public class OtherClientsInfoMessageSender implements OtherClientsInfoDelegate {

    private final String identifier;
    private final Map<String, CoordinatorClient> otherClientsCoordinators;
    private final Random random = new Random();

    private CoordinatorClient getRandomCoordinatorClient() {
        Object[] values = otherClientsCoordinators.values().toArray();
        System.out.println(otherClientsCoordinators);
        return (CoordinatorClient) values[random.nextInt(values.length)];
    }

    @Override
    public void askOtherClientsForResponse(String lastVotingUUID) {
        var randomClient = getRandomCoordinatorClient();
        var message = new Message(MessageType.REQUEST_FOR_INFORMATION, RequestForInformation.of(identifier, lastVotingUUID));
        randomClient.sendMessage(message);
    }

    @Override
    public void sendInformationAboutState(String identifier, int value, boolean isCommited) {
        var message = new Message(MessageType.REQUEST_FOR_INFORMATION_RESPONSE, RequestForInformationResponse.of(value, isCommited));
        otherClientsCoordinators.get(identifier).sendMessage(message);
    }
}
