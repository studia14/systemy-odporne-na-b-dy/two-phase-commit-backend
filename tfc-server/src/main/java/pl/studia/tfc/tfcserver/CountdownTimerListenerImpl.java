package pl.studia.tfc.tfcserver;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.entity.CountdownTimerListener;
import pl.studia.tfc.tfcserver.tcp.CoordinatorClient;

import java.util.Map;
import java.util.Random;

import static pl.studia.tfc.entity.CountdownTimer.countdownTimerLength;

@RequiredArgsConstructor
public class CountdownTimerListenerImpl implements CountdownTimerListener {

    private final Map<String, CoordinatorClient> otherClientCoordinators;
    private final Random random = new Random();
    private final String identifier;
    @Setter
    private String lastVoteIdentifier;

    private final ServerController serverController;

    @Override
    public void onCountdownCancel() {

    }

    @Override
    public void onNormalCountdownEnd() {
        serverController.timeoutOccurred();

    }

    @Override
    public void onCountdownStep(int timer) {
        System.out.println("Server countdown: " + timer);
        serverController.updateCountdown((double) timer / countdownTimerLength);
    }
}
