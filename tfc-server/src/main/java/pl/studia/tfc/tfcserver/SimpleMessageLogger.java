package pl.studia.tfc.tfcserver;

import pl.studia.tfc.entity.MessageLogger;

public class SimpleMessageLogger implements MessageLogger {
    @Override
    public void logMessage(String message) {
        System.out.println(message);
    }
}
