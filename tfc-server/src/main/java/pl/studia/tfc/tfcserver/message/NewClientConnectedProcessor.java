package pl.studia.tfc.tfcserver.message;

import lombok.RequiredArgsConstructor;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.NewClientInfo;
import pl.studia.tfc.entity.processor.MessageProcessor;
import pl.studia.tfc.tfcserver.tcp.CoordinatorClient;
import pl.studia.tfc.tfcserver.tcp.ServerClientListener;

import java.net.Socket;
import java.util.Map;

@RequiredArgsConstructor
public class NewClientConnectedProcessor implements MessageProcessor {

    private final Map<String, CoordinatorClient> otherClientCoordinators;

    @Override
    public <T extends Message> void processMessage(T message, Socket source) {
        var messageBody = (NewClientInfo)message.getData();

        Thread t = new Thread(() -> {
            var coordinatorClient = new CoordinatorClient(Map.of(),
                    new ServerClientListener());
            coordinatorClient.connect(messageBody.getIpAddress(), messageBody.getPort());
            otherClientCoordinators.put(messageBody.getIdentifier(), coordinatorClient);
        });
        t.start();
        System.out.println("New client connected: " + messageBody);
    }
}
