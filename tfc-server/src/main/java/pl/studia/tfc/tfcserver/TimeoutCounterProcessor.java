package pl.studia.tfc.tfcserver;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.entity.CountdownTimer;
import pl.studia.tfc.entity.CountdownTimerListener;
import pl.studia.tfc.tfcserver.delegate.GuiDelegate;

import java.util.Optional;

@RequiredArgsConstructor
public class TimeoutCounterProcessor implements TimeoutCounterDelegate, CountdownTimerListener {

    private final int countdownTimerLength = 30;

    private CountdownTimer countdownTimer = new CountdownTimer(this, countdownTimerLength);
    @Setter
    private VotingListener votingListener;
    @Setter
    private GuiDelegate guiDelegate;

    @Override
    public void onCountdownCancel() {
        countdownTimer.cancel();
    }

    @Override
    public void onNormalCountdownEnd() {
        Optional.ofNullable(votingListener).ifPresent(VotingListener::timeout);
    }

    @Override
    public void onCountdownStep(int timer) {
        guiDelegate.updateCountdown((float)timer/countdownTimerLength);
        votingListener.timeoutStep();
    }

    @Override
    public void startCounter() {
        if (!countdownTimer.isActive()) {
            countdownTimer = new CountdownTimer(this, countdownTimerLength);
            countdownTimer.start();
        }
    }

    @Override
    public void stopCounter() {
        countdownTimer.cancel();
    }
}
