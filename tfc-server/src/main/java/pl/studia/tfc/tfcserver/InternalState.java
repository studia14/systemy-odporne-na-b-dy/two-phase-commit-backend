package pl.studia.tfc.tfcserver;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.tfcserver.delegate.GuiDelegate;
import pl.studia.tfc.tfcserver.delegate.OtherClientsInfoDelegate;
import pl.studia.tfc.tfcserver.delegate.PropertiesChangeListener;
import pl.studia.tfc.tfcserver.delegate.VotingDelegate;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Getter
@RequiredArgsConstructor
public class InternalState implements PropertiesChangeListener, OtherClientsInfoListener {
    private int valueProposition = -1;
    private int value = -1;
    private String lastVotingUUID;
    private boolean voteStarted = false;
    private boolean receiveCoordinatorDecision = false;
    @Setter
    private boolean acceptVoting = true;
    @Setter
    private boolean blockVoting = false;
    private boolean hangup = false;
    private boolean hangupChanged = false;
    private boolean lastVotingCommited = false;
    private boolean ignoreGlobalAcceptRejectMessage = false;
    private final VotingDelegate votingDelegate;
    private final TimeoutCounterDelegate timecoutCounterDelegate;
    private final OtherClientsInfoDelegate otherClientsInfoDelegate;
    @Setter
    private GuiDelegate guiDelegate;

    private final Random random = new Random();

    public void vote(int stateProposition, String votingUUID) {
        receiveCoordinatorDecision = false;
        this.valueProposition = stateProposition;
        this.lastVotingUUID = votingUUID;
        if (!blockVoting) {
            guiDelegate.startVoting(valueProposition);
            timecoutCounterDelegate.startCounter();
            Executors.newSingleThreadScheduledExecutor().schedule(() -> {
                if (!hangup) {
                    voteStarted = true;
                    this.lastVotingCommited = false;
                    if (acceptVoting) {
                        votingDelegate.voteForCommit();
                    } else {
                        votingDelegate.voteForReject();
                    }
                }

            }, random.nextInt(9000), TimeUnit.MILLISECONDS);
        }
    }

    void globalAccept() {
        voteStarted = false;
        if(!hangup && !ignoreGlobalAcceptRejectMessage) {
            this.receiveCoordinatorDecision = true;
            this.value = this.valueProposition;
            this.lastVotingCommited = true;
            guiDelegate.gotGlobalCommit(value);
            timecoutCounterDelegate.stopCounter();
        }

    }

    void globalReject() {
        voteStarted = false;
        if(!hangup && !ignoreGlobalAcceptRejectMessage) {
            this.receiveCoordinatorDecision = true;
            guiDelegate.gotGlobalReject();
            this.lastVotingCommited = false;
            timecoutCounterDelegate.stopCounter();
        }

    }

    void timeout() {
        otherClientsInfoDelegate.askOtherClientsForResponse(lastVotingUUID);
    }

    void timeoutStep() {
        if (!hangup && hangupChanged) {
           this.hangupChanged = false;
           otherClientsInfoDelegate.askOtherClientsForResponse(lastVotingUUID);
        }
    }

    @Override
    public void acceptVoting(boolean value) {
        this.acceptVoting = value;
    }

    @Override
    public void blockRequestForVotingMessage(boolean value) {
        this.blockVoting = value;
    }

    @Override
    public void ignoreGlobalAcceptRejectMessage(boolean value) {
        this.ignoreGlobalAcceptRejectMessage = value;
    }

    @Override
    public void hangUp(boolean value) {
        this.hangup = value;
        this.hangupChanged = true;
        if (!this.hangup) {
            if(!voteStarted && !receiveCoordinatorDecision) {
                votingDelegate.voteForReject();
            }
            else if(voteStarted && !receiveCoordinatorDecision){
                vote(valueProposition, lastVotingUUID);
            }
        }
    }

    @Override
    public void receivedRequestInfo(String identifier, String voteIdentifier) {
        if (voteIdentifier.equals(lastVotingUUID)) {
           otherClientsInfoDelegate.sendInformationAboutState(identifier, value, lastVotingCommited);
        }
    }

    @Override
    public void messageCommited() {
        this.lastVotingCommited = true;
        this.valueProposition = value;
        guiDelegate.gotGlobalCommit(this.valueProposition);
    }

    @Override
    public void messageRejected() {
        this.lastVotingCommited = false;
        guiDelegate.gotGlobalReject();
    }
}
