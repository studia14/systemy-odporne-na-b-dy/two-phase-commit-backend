package pl.studia.tfc.tfcserver;

public interface OtherClientsInfoListener {
    void receivedRequestInfo(String identifier, String voteIdentifier);
    void messageCommited();
    void messageRejected();
}
