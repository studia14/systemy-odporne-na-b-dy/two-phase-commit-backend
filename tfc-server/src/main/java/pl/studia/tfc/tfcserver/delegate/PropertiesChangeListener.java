package pl.studia.tfc.tfcserver.delegate;

public interface PropertiesChangeListener {
    void acceptVoting(boolean value);
    void blockRequestForVotingMessage(boolean value);
    void ignoreGlobalAcceptRejectMessage(boolean value);
    void hangUp(boolean value);
}
