package pl.studia.tfc.tfcserver.delegate;

public interface VotingDelegate {
    void voteForCommit();
    void voteForReject();
}
