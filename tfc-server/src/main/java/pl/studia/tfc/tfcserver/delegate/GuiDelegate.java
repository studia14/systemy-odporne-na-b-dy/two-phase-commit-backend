package pl.studia.tfc.tfcserver.delegate;

public interface GuiDelegate {
    void startVoting(int newValue);
    void gotGlobalCommit(int newValue);
    void gotGlobalReject();
    void doneVoting();
    void updateCountdown(double timeLeftPercent);
    void timeoutOccurred();
}
