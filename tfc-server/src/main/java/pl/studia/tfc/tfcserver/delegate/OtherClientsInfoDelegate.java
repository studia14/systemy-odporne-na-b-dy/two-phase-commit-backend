package pl.studia.tfc.tfcserver.delegate;

public interface OtherClientsInfoDelegate {
    void askOtherClientsForResponse(String voteUUID);
    void sendInformationAboutState(String identifier, int value, boolean isCommited);
}
