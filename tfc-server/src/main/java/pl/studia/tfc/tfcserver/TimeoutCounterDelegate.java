package pl.studia.tfc.tfcserver;

public interface TimeoutCounterDelegate {
    void startCounter();
    void stopCounter();
}
