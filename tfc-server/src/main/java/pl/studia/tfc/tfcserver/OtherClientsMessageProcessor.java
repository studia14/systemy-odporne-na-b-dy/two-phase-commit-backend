package pl.studia.tfc.tfcserver;

import lombok.Setter;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.RequestForInformation;
import pl.studia.tfc.entity.RequestForInformationResponse;
import pl.studia.tfc.entity.processor.MessageProcessor;

import java.net.Socket;
import java.util.Optional;

public class OtherClientsMessageProcessor implements MessageProcessor {

    @Setter
    private OtherClientsInfoListener otherClientsInfoListener;

    @Override
    public <T extends Message> void processMessage(T message, Socket source) {
        if (message.getMessageType().equals(MessageType.REQUEST_FOR_INFORMATION)) {
            var messageBody = (RequestForInformation)message.getData();
            Optional.ofNullable(otherClientsInfoListener).ifPresent(listener -> {
                listener.receivedRequestInfo(messageBody.getIdentifier(), messageBody.getVoteIdentifier());
            });
        }
        if (message.getMessageType().equals(MessageType.REQUEST_FOR_INFORMATION_RESPONSE)) {
            var messageBody = (RequestForInformationResponse)message.getData();
            if (messageBody.isCommited()) {
                Optional.ofNullable(otherClientsInfoListener).ifPresent((OtherClientsInfoListener::messageCommited));
            }
            else {
                Optional.ofNullable(otherClientsInfoListener).ifPresent((OtherClientsInfoListener::messageRejected));
            }
        }
    }
}
