package pl.studia.tfc.tfcserver;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.studia.tfc.entity.Message;
import pl.studia.tfc.entity.MessageType;
import pl.studia.tfc.entity.VoteRequest;
import pl.studia.tfc.entity.processor.MessageProcessor;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class VotingMessageProcessor implements MessageProcessor {

    private List<VotingListener> votingListeners = new ArrayList<>();

    public void addVotingListener(VotingListener votingListener) {
        votingListeners.add(votingListener);
    }

    @Override
    public <T extends Message> void processMessage(T message, Socket source) {
        if (message.getMessageType() == MessageType.GLOBAL_COMMIT) {
            votingListeners.forEach(VotingListener::globalAccept);
        }
        else if(message.getMessageType() == MessageType.GLOBAL_REJECT) {
            votingListeners.forEach(VotingListener::globalReject);
        }
        else if(message.getMessageType() == MessageType.VOTE_REQUEST) {
            var newValue = ((VoteRequest) message.getData()).getMessage();
            var lastVoteIdentifier = ((VoteRequest) message.getData()).getVoteIdentifier();
            votingListeners.forEach(votingListener -> {
                votingListener.receiveVotingRequest(newValue, lastVoteIdentifier);
            });
        }
    }
}
