module tfc.server {
    requires javafx.controls;
    requires javafx.fxml;
    requires tfc.common;
    requires static lombok;

    opens pl.studia.tfc.tfcserver to javafx.fxml;
    exports pl.studia.tfc.tfcserver;
    exports pl.studia.tfc.tfcserver.delegate;
    opens pl.studia.tfc.tfcserver.delegate to javafx.fxml;
    exports pl.studia.tfc.tfcserver.tcp;
    opens pl.studia.tfc.tfcserver.tcp to javafx.fxml;
}